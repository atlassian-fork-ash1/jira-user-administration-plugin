package com.atlassian.jira.plugins.useradmin.rest;

import com.atlassian.crowd.exception.*;
import com.atlassian.jira.plugins.useradmin.service.UserFilter;
import com.atlassian.jira.plugins.useradmin.service.UserManagerService;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("users")
@Consumes ({ MediaType.APPLICATION_JSON })
@Produces ({ MediaType.APPLICATION_JSON })
public class UserResource {

    private final UserManagerService userManagerService;

    public UserResource(final UserManagerService userManagerService) {
        this.userManagerService = userManagerService;
    }

    @GET
    @AnonymousAllowed
    @Path("user")
    public Response getUser(@QueryParam("username") final String username) {
        if (username != null) {
            return Response.ok(userManagerService.getUserByName(username)).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @GET
    @Path("filter")
    @AnonymousAllowed
    public Response getUsers(@QueryParam("groups") final List<String> groups,
                             @QueryParam("roles") final List<String> roles,
                             @QueryParam("projects") final List<String> projects,
                             @QueryParam("userNames") final List<String> userNames,
                             @DefaultValue("0") @QueryParam("startAt") final int startAt,
                             @DefaultValue("50") @QueryParam("limit") final int limit) {
        if (startAt < 0 || limit < 0) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        return Response.ok(userManagerService.getUsers(UserFilter.createFilter(groups, roles, projects, userNames), startAt, limit)).build();
    }

    @PUT
    @Path("groups")
    @AnonymousAllowed
    public Response addToGroups(@QueryParam("users") final List<String> userNames, @QueryParam("groups") final List<String> groupNames) {
        //todo: convert query params to params
        if (userNames == null || groupNames == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        try {
            userManagerService.addUsersToGroups(userNames, groupNames);
            return Response.ok().build();
        } catch (OperationNotPermittedException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } catch (UserNotFoundException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } catch (GroupNotFoundException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } catch (OperationFailedException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
    @PUT
    @Path("degrade")
    @AnonymousAllowed
    public Response removeFromGroups(@QueryParam("users") final List<String> userNames, @QueryParam("groups") final List<String> groupNames) {
        //todo: convert query params to params
        if (userNames == null || groupNames == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        try {
            userManagerService.removeUsersFromGroups(userNames, groupNames);
            return Response.ok().build();
        } catch (Exception e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @POST
    @Path("user")
    @AnonymousAllowed
    public Response createUser(@QueryParam("username") final String username,
                               @QueryParam("password") final String password,
                               @QueryParam("full-name") final String fullName,
                               @QueryParam("email") final String email,
                               @QueryParam("send-notification") final boolean sendNotification) {
        try {
            userManagerService.addUser(username, password, fullName, email, sendNotification);
            return Response.ok().build();
        } catch (OperationNotPermittedException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } catch (InvalidUserException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        } catch (InvalidCredentialException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

}
