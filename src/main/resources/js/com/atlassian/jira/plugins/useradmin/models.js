(function($){
    AJS.namespace("AJS.Plugins.useradmin.models");

    var UserModel = Backbone.Model.extend({

        defaults : {
            selected : false
        },

        idAttribute : "name",
        url : function(){
            return contextPath + "/rest/user-administration/1.0/users/user?username=" + this.id
        }
    });

    var UserCollection = Backbone.Collection.extend({

        model: UserModel
    });

    var SelectedUsersCollection = Backbone.Collection.extend({
        model: UserModel
    });


    var UserSelectionModel = Backbone.Model.extend({

        initialize : function(){
            this.users = new SelectedUsersCollection();
        },

        _modifyGroups : function(groupNames, operation){
            if(!_.isArray(groupNames)){
                groupNames = [groupNames];
            }

            if(groupNames.length == 0 || this.users.length ==0){
                return;
            }

            var ajaxData = [];

            this.users.each(function(m){
                ajaxData.push({name : "users",value :m.id});
            });

            _.each(groupNames, function(m){
                ajaxData.push({name : "groups",value :m});
            });

            var that = this;

            $.ajax({
                url : contextPath + "/rest/user-administration/1.0/users/"+operation+"?" +$.param(ajaxData),
                type : "PUT",
//                data : ajaxData,
                success : function(){
                    that.users.each(function(m){
                        m.fetch();
                    });
                }
            });
        },


        updateGroups : function(selectedGroups){
            var allGroups = _.intersection.apply(this, this.users.pluck("groupNames"));
            var groupsToRemove = _.difference(allGroups, selectedGroups);
            console.log("Removing: ");
            console.log(groupsToRemove);
            this.degradeUsers(groupsToRemove);
            this.promoteUsers(selectedGroups);

        },

        promoteUsers : function(groupNames){
            this._modifyGroups(groupNames, "groups");
        },

        degradeUsers : function(groupNames){
            this._modifyGroups(groupNames, "degrade");
        }
    });

    var FilterModel = Backbone.Model.extend({
        defaults : {
            userNames : "",
            groups : [],
            roles : [],
            projects : [],
            startAt : 0,
            limit : 10
        }
    });

    var UserTable = Backbone.Model.extend({


        defaults : {
            resultsCount : -1
        },

        initialize : function(){
            this.users = new UserCollection();
            this.users.bind("reset", this.restoreSelection, this);
            this.selectedUsers = new UserSelectionModel();
            this.filterModel = new FilterModel();
            this.filterModel.bind("change", this.filterChanged, this);
        },

        filterChanged : function(){
            this.fetch();
        },

        restoreSelection : function(){
            var selected = this.selectedUsers.users;
            this.users.each(function(elem){
                if(selected.get(elem.id)){
                    elem.set("selected", true);
                }
            });
        },

        selectUser : function(userModel){
            if(this.selectedUsers.users.length > 1){
                this.addUserToSelection(userModel);
            } else {
                this.replaceSelection(userModel);
            }
        },

        replaceSelection : function(newSelection){
            var su = this.selectedUsers.users;

            //we have to pass cloned collection as we are removing from the same collection
            var selectedUsersCopy = new su.constructor(su.models);
            this._toggleSelectUsersCollection(selectedUsersCopy, false);

            su.add(newSelection);
            newSelection.set("selected", true);

            this.trigger("change:selection", this.selectedUsers);
        },

        addUserToSelection : function(userModel){
            var su = this.selectedUsers.users;

            if(su.get(userModel.id)){
                su.remove(userModel.id);
                userModel.set("selected", false);
            } else {
                su.add(userModel);
                userModel.set("selected", true);

            }

            this.trigger("change:selection", this.selectedUsers);
        },

        selectAllToggle : function(newValue){
            this._toggleSelectUsersCollection(this.users, newValue);
        },

        _toggleSelectUsersCollection : function(users, newValue){
            var su = this.selectedUsers.users;

            users.each(function(userModel){
                if(newValue){
                    su.add(userModel);
                } else {
                    su.remove(userModel.id);
                }
                userModel.set("selected", newValue);
            });

            this.trigger("change:selection", this.selectedUsers);
        },

        selectAllResultsToggle : function(newValue){
            //get all users
            var all = new UserTable();
            var filter = this.filterModel.toJSON();
            filter.startAt = 0;
            filter.limit = null;
            all.filterModel.set(filter, {silent: true});
            var that = this;

            all.fetch({
                success: function(){
                    //todo: handling re-fetching second time the same model
                    //temporary walkaround - add currently displayed users to all users collection

                    all.users.remove(that.users.models);
                    all.users.add(that.users.models);

                    that._toggleSelectUsersCollection(all.users, newValue);
                }
            });
        },

        url : function() {

            var params = [];
            var that = this;

            function addFiterToParams(name, value){
                params.push({
                    name : name,
                    value : value
                });
            }

            //foreach filter model param,  add it to query array
            _.each(this.filterModel.attributes, function (attribute, name){
                if(_.isArray(attribute)){
                    _.each(attribute, function(val){
                        addFiterToParams(name, val);
                    });
                } else if(!_.isNull(attribute)) {
                    addFiterToParams(name, attribute);
                }
            });

            var uriString = $.param(params);
            return contextPath + "/rest/user-administration/1.0/users/filter?"+uriString;
        },
        parse :function(data) {
            this.users.reset(data.users);
            return _.pick(data, "resultsCount");
        },
        fetch : function(options){
            var that = this;
            UserTable.__super__.fetch.apply(this, [{
                success: function(){
                    that.trigger("fetch");
                    if(options && options.success){
                        options.success();
                    }
                }
            }]);
        }
    });

    AJS.Plugins.useradmin.models.user = UserModel;
    AJS.Plugins.useradmin.models.userSelection = UserSelectionModel;
    AJS.Plugins.useradmin.models.userCollection = UserCollection;
    AJS.Plugins.useradmin.models.userTable = UserTable;



})(AJS.$);