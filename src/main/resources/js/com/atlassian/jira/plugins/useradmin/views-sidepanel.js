(function($){
    AJS.namespace("AJS.Plugins.useradmin.views.sidepanel");

    var SidePanelContentView = Backbone.View.extend({
       groupsSubmit : function(e){
            e.preventDefault();
            this.model.updateGroups(this.$intersectGroupsSelector.val());
        }
    });


    var UserView = SidePanelContentView.extend({

        events : {
            "submit .intersect-groups" : "groupsSubmit"
        },

        initialize : function() {
            this.usermodel = this.model.users.first();
            this._initializeEvents();
        },

        _initializeEvents: function() {
            this.usermodel.bind('reset', this.render, this);
        },


        render : function(){
            var content = AJS.Plugins.useradmin.templates.singleUser({
                user:this.usermodel.attributes,
                token : atl_token()
            });
            this.$el.html(content);

            this.$intersectGroupsSelector = this.$el.find(".intersect-groups select");

            var f= new AJS.MultiSelect({
                element: this.$intersectGroupsSelector,
                itemAttrDisplayed: "label"
            });
            setTimeout(function(){f.updateItemsIndent();}, 0);


            return this;
        },

        remove : function(){
            this.usermodel.off(null, null, this);
            this.$el.remove();
        }

    });


    var CommonUsersDataView = SidePanelContentView.extend({
        events : {
            "click .common-group" : "commonGroupClicked",
//            "unselect .intersect-groups select" : "removeUserGroup",
//            "selected .intersect-groups select" : "addUserGroup",
            "submit .intersect-groups" : "groupsSubmit"
        },

        addUserGroup : function(e, selected){
            this.model.promoteUsers(selected.properties.value);
        },

        removeUserGroup : function(e, selected){
            this.model.degradeUsers(selected.properties.value);
        },


        initialize : function(){
            this.model.users.bind("change", this.render, this);
            this.bind("visible", this.onContentVisible, this);
        },

        commonGroupClicked : function(e){
            e.preventDefault();
            var element = $(e.target);
            this.model.promoteUsers(element.data("name"));
        },

        onContentVisible : function(){

            //Shortener is the most evil control I've ever seen...
            var shortenerWithoutPersistence = AJS.Shortener.extend({
                _saveState: function(value) {

                }
            });
            new shortenerWithoutPersistence({
                element: this.$el.find("h2"),
                numRows: 1,
                persist : false
            });
        },

        render : function() {
            var data = {};
            var users = this.model.users;
            data.names =  users.pluck("displayName");
            data.intersectGroups = _.intersection.apply(_, users.pluck("groupNames"));
            data.notIntersectGroups = _.difference(this.model.get("allGroups"), data.intersectGroups);

            data.commonGroups = _.map(_.difference(_.union.apply(_, users.pluck("groupNames")), data.intersectGroups), function(groupName){
                return {
                    name : groupName,
                    count : users.reduce(function(sum, user){
                        if(_.contains(user.get("groupNames"), groupName)){
                            return sum+1;
                        } else {
                            return sum;
                        }
                    }, 0)

                };
            });

            var content = AJS.Plugins.useradmin.templates.multiUser({
                users:data,
                token : atl_token()
            });
            this.$el.html(content);


            this.$intersectGroupsSelector = this.$el.find(".intersect-groups select");

            var f = new AJS.MultiSelect({
                element: this.$intersectGroupsSelector,
                itemAttrDisplayed: "label"
            });
            setTimeout(function(){f.updateItemsIndent();}, 0);

            return this;
        }
    });


    var SidePanel = Backbone.View.extend({

        initialize : function(){
            this.setElement($("#useradmin-sidebar")[0]);
            this.options.tableModel.on("change:selection", this.selectionChanged, this);
            this.options.contentView = null;
        },

        selectionChanged : function(selectedUsersModel){
            var selectedUsers = selectedUsersModel.users;
            switch(selectedUsers.length){
                case 0:
                    this.removeContentView();
                    break;
                case 1:
                    this.switchToSingleUserMode(selectedUsersModel);
                    break;
                default:
                    this.switchToMultiUserMode(selectedUsersModel);
            }
        },

        setContentView : function(view){
            this.removeContentView();
            this.options.contentView = view;
            view.render();
            this.$el.append(view.$el);
            view.trigger("visible");
        },

        removeContentView : function(){
            if(this.options.contentView){
                this.options.contentView.remove();
            }
        },

        switchToSingleUserMode : function(model){
            var view = new UserView({model: model});
            this.setContentView(view);
        },
        switchToMultiUserMode : function(model){
            var view = new CommonUsersDataView({model : model});
            this.setContentView(view);
        }
    });


    AJS.Plugins.useradmin.views.sidepanel.user = UserView;
    AJS.Plugins.useradmin.views.sidepanel.commonData = CommonUsersDataView;
    AJS.Plugins.useradmin.views.sidepanel.panel = SidePanel;

})(AJS.$);